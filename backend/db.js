const mongoose = require("mongoose");

mongoose.connect('mongodb://localhost:27017/sekolah',(err) => {
    if(!err)
        console.log("Connected");
    else
        console.log("Error in DB Connection -> " + JSON.stringify(err,undefined,2));
});

module.exports = mongoose;