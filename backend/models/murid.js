const mongoose = require('mongoose');

var Murid = mongoose.model('murid', {
    nis: {
        type: String
    },
    name: {
        type: String
    },
    tgllhr: {
        type: Date
    },
    kelas: {
        type: String
    },
    spp: {
        type: Number
    }
});

module.exports = {
    Murid
};