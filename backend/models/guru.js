const mongoose = require('mongoose');

var Guru = mongoose.model('guru', {
    nik: {
        type: String
    },
    name: {
        type: String
    },
    tgllhr: {
        type: Date
    },
    jabatan: {
        type: String
    },
    gaji: {
        type: Number
    }
});

module.exports = {
    Guru
};