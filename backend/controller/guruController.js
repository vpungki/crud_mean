const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Guru } = require('../models/guru');
//sama dg line 3 dan 22 di guru model

// => localhost:2222/guru/
router.get('/', (req, res) => {
    Guru.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Guru :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        Guru.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Guru :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var gru = new Guru({
        nik: req.body.nik,
        name: req.body.name,
        tgllhr: req.body.tgllhr,
        jabatan: req.body.jabatan,
        gaji: req.body.gaji,
    });
    gru.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Guru Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var gru = {
        nik: req.body.nik,
        name: req.body.name,
        tgllhr: req.body.tgllhr,
        jabatan: req.body.jabatan,
        gaji: req.body.gaji,
    };
    Guru.findByIdAndUpdate(req.params.id, { $set: gru }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Guru Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        Guru.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Guru Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;