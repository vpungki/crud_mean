const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Murid } = require('../models/murid');
//sama dg line 3 dan 22 di murid model

// => localhost:2222/murid/
router.get('/', (req, res) => {
    Murid.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Murid :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        Murid.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Murid :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var mrd = new Murid({
        nis: req.body.nis,
        name: req.body.name,
        tgllhr: req.body.tgllhr,
        kelas: req.body.kelas,
        spp: req.body.spp,
    });
    mrd.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Murid Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var mrd = {
        nis: req.body.nis,
        name: req.body.name,
        tgllhr: req.body.tgllhr,
        kelas: req.body.kelas,
        spp: req.body.spp,
    };
    Murid.findByIdAndUpdate(req.params.id, { $set: mrd }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Murid Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        Murid.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Murid Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;