const express = require('express');
const bodyParse = require('body-parser');
const cors = require('cors');

const {
    mongoose
} = require('./db.js');
var guruController = require('./controller/guruController.js');
var muridController = require('./controller/muridController.js');

var app = express();
app.use(bodyParse.json());
app.use(cors({
    origin: 'http://localhost:4200'
}));

app.listen(2222, () => console.log("Server start in port 2222"));

app.use('/guru', guruController);
app.use('/murid', muridController);