export class Murid {
    _id: String;
    nis: String;
    name: String;
    tgllhr: Date;
    kelas: String;
    spp: Number;
}