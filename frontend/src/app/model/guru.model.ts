export class Guru {
    _id: String;
    nik: String;
    name: String;
    tgllhr: Date;
    jabatan: String;
    gaji: Number;
}