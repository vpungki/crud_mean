import { Component, OnInit } from '@angular/core';
import { ServiceService } from "../service.service";
import { Router } from "@angular/router"

@Component({
  selector: 'app-siswa',
  templateUrl: './siswa.component.html',
  styleUrls: ['./siswa.component.css']
})
export class SiswaComponent implements OnInit {

  murid_arr : any;
  murid_from_db : any;
  constructor(private service:ServiceService, private router: Router) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.service.getMurid().subscribe(data=>{
      console.log(JSON.stringify(data))
      return this.murid_from_db = data
    });
  }

  getCurrentMurid(murid):void{
    console.log(murid);
    alert("Nama: "+murid.name+", Kelas : "+murid.kelas)
  }

  addMurid(): void{
    this.router.navigate(["murid/add-murid"])
  }

  editMurid(murid): void{
    console.log(murid);
    window.localStorage.removeItem("muridId");
    window.localStorage.setItem("editMuridId", murid._id+"");
    this.router.navigate(["murid/edit-murid"])
  }

  deleteMurid(murid): void{
    let x=confirm("Hapus "+murid.name+" secara permanen dari DataBase ?")
    if(x){
      this.service.deleteMurid(murid).subscribe((res)=>{
        this.refresh();
      })
    }
    
  }

}
