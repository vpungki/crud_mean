import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service.service';
import { FormBuilder, FormGroup, Validator, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-siswa',
  templateUrl: './edit-siswa.component.html',
  styleUrls: ['./edit-siswa.component.css']
})
export class EditSiswaComponent implements OnInit {

  editForm: FormGroup;
  constructor(private router: Router, private serviceService: ServiceService, private formBuilder: FormBuilder) { }

  ngOnInit() {
  let muridId = window.localStorage.getItem("editMuridId");

    if (!muridId) {
      this.router.navigate(['murid']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id: [],
      nis: [],
      name: [],
      tgllhr: [],
      kelas: [],
      spp: [],
      __v: []
    });

    this.serviceService.getMuridById(muridId).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this.serviceService.putMurid(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Sukses");
        window.localStorage.removeItem("muridId");
        this.router.navigate(['murid']);
      } catch (err) {
        console.log(err.message);
      }
    })
  }

}
