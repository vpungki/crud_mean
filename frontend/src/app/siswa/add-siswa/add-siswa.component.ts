import { Component, OnInit } from '@angular/core';
import { ServiceService } from "../../service.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-siswa',
  templateUrl: './add-siswa.component.html',
  styleUrls: ['./add-siswa.component.css']
})
export class AddSiswaComponent implements OnInit {

  murid_arr : any;
  murid_from_db : any;
  constructor(private service:ServiceService, private router: Router) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.service.getMurid().subscribe(data=>{
      console.log(JSON.stringify(data))
      return this.murid_from_db = data
    });
  }

  onSubmit(){
    this.murid_arr={
      "nis" :(document.getElementById("nis") as HTMLInputElement).value,
      "name" :(document.getElementById("name") as HTMLInputElement).value,
      "tgllhr" :(document.getElementById("tgllhr") as HTMLInputElement).value,
      "kelas" :(document.getElementById("kelas") as HTMLInputElement).value,
      "spp" :(document.getElementById("spp") as HTMLInputElement).value,
    }

    this.service.postMurid(this.murid_arr).subscribe((res)=>{
      console.log(res);
      console.log(this.murid_arr);
      // $('#modalwindow').modal('hide');
      this.refresh();
      this.addMurid();

    });
  }
  addMurid(): void{
    this.router.navigate(["murid/"])
  }

}
