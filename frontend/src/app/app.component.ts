import { Component } from '@angular/core';
import { ServiceService } from "./service.service";
import { Router } from "@angular/router"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private service:ServiceService, private router: Router) { }

  guru(): void{
    this.router.navigate(["guru/"])
  }

  murid(): void{
    this.router.navigate(["murid/"])
  }
}
