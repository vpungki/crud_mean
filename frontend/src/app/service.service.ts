import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Guru } from './model/guru.model'
import { Murid } from './model/murid.model'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServiceService {

  BASE_URL:string='http://localhost:2222/';
  constructor(private http:HttpClient) { }

  getGuru(){
    return this.http.get(this.BASE_URL+'guru');
  }

  getGuruById(gruId: String):Observable<any>{
    return this.http.get<any>(this.BASE_URL+'guru/'+gruId);
  }

  postGuru(gru: Guru){
    return this.http.post(this.BASE_URL+'guru',gru);
  }

  deleteGuru(gru: Guru){
    return this.http.delete(this.BASE_URL+'guru/'+gru._id);
  }

  putGuru(gru: Guru){
    return this.http.put(this.BASE_URL+'guru/'+gru._id, gru);
  }

  // =================================================================

  getMurid(){
    return this.http.get(this.BASE_URL+'murid');
  }

  getMuridById(mrdId: String):Observable<any>{
    return this.http.get<any>(this.BASE_URL+'murid/'+mrdId);
  }

  postMurid(mrd: Murid){
    return this.http.post(this.BASE_URL+'murid',mrd);
  }

  deleteMurid(mrd: Murid){
    return this.http.delete(this.BASE_URL+'murid/'+mrd._id);
  }

  putMurid(mrd: Murid){
    return this.http.put(this.BASE_URL+'murid/'+mrd._id, mrd);
  }

}
