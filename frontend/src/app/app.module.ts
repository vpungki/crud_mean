import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';


import { AppComponent } from './app.component';
import { SiswaComponent } from './siswa/siswa.component';
import { GuruComponent } from './guru/guru.component';
import { AddGuruComponent } from './guru/add-guru/add-guru.component';
import { EditGuruComponent } from './guru/edit-guru/edit-guru.component';
import { ServiceService } from './service.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddSiswaComponent } from './siswa/add-siswa/add-siswa.component';
import { EditSiswaComponent } from './siswa/edit-siswa/edit-siswa.component';

const routes:Routes=[
  {
    path:'guru', 
    component:GuruComponent
  },
  {
    path:'guru/add-guru',
    component:AddGuruComponent
  },
  {
    path:'guru/edit-guru',
    component:EditGuruComponent
  },
  {
    path:'murid', 
    component:SiswaComponent
  },
  {
    path:'murid/add-murid', 
    component:AddSiswaComponent
  },
  {
    path:'murid/edit-murid', 
    component:EditSiswaComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    SiswaComponent,
    GuruComponent,
    AddGuruComponent,
    EditGuruComponent,
    AddSiswaComponent,
    EditSiswaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[RouterModule],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
