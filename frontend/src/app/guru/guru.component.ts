import { Component, OnInit } from '@angular/core';
import { ServiceService } from "../service.service";
import { Router } from "@angular/router"
// import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-guru',
  templateUrl: './guru.component.html',
  styleUrls: ['./guru.component.css']
})
export class GuruComponent implements OnInit {

  guru_arr : any;
  guru_from_db : any;
  constructor(private service:ServiceService, private router: Router) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.service.getGuru().subscribe(data=>{
      console.log(JSON.stringify(data))
      return this.guru_from_db = data
    });
  }

  getCurrentGuru(guru):void{
    console.log(guru);
    alert("Nama: "+guru.name+", Jabatan : "+guru.jabatan)
  }

  // showGuruModal():void {
  //   this.guruM
  // }

  // hideChildModal():void {
  //   this.childModal.hide();
  //  }
  
  onSubmit(){
    this.guru_arr={
      "nik" :(document.getElementById("nik") as HTMLInputElement).value,
      "name" :(document.getElementById("name") as HTMLInputElement).value,
      "tgllhr" :(document.getElementById("tgllhr") as HTMLInputElement).value,
      "jabatan" :(document.getElementById("jabatan") as HTMLInputElement).value,
      "gaji" :(document.getElementById("gaji") as HTMLInputElement).value,
    }

    this.service.postGuru(this.guru_arr).subscribe((res)=>{
      console.log(res);
      console.log(this.guru_arr);
      // $('#modalwindow').modal('hide');
      this.refresh();
    });
  }

  addGuru(): void{
    this.router.navigate(["guru/add-guru"])
  }

  editGuru(guru): void{
    console.log(guru);
    window.localStorage.removeItem("guruId");
    window.localStorage.setItem("editGuruId", guru._id+"");
    this.router.navigate(["guru/edit-guru"])
  }

  deleteGuru(guru): void{
    let x=confirm("Hapus "+guru.name+" secara permanen dari DataBase ?")
    if(x){
      this.service.deleteGuru(guru).subscribe((res)=>{
        this.refresh();
      })
    }
  }

}
