import { Component, OnInit } from '@angular/core';
import { ServiceService } from "../../service.service";
import { Router } from "@angular/router"

@Component({
  selector: 'app-add-guru',
  templateUrl: './add-guru.component.html',
  styleUrls: ['./add-guru.component.css']
})
export class AddGuruComponent implements OnInit {

  guru_arr : any;
  guru_from_db : any;
  constructor(private service:ServiceService, private router: Router) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.service.getGuru().subscribe(data=>{
      console.log(JSON.stringify(data))
      return this.guru_from_db = data
    });
  }

  onSubmit(){
    this.guru_arr={
      "nik" :(document.getElementById("nik") as HTMLInputElement).value,
      "name" :(document.getElementById("name") as HTMLInputElement).value,
      "tgllhr" :(document.getElementById("tgllhr") as HTMLInputElement).value,
      "jabatan" :(document.getElementById("jabatan") as HTMLInputElement).value,
      "gaji" :(document.getElementById("gaji") as HTMLInputElement).value,
    }

    this.service.postGuru(this.guru_arr).subscribe((res)=>{
      console.log(res);
      console.log(this.guru_arr);
      // $('#modalwindow').modal('hide');
      this.refresh();
      this.addGuru();

    });
  }

  addGuru(): void{
    this.router.navigate(["guru/"])
  }
}
