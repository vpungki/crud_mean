import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGuruComponent } from './edit-guru.component';

describe('EditGuruComponent', () => {
  let component: EditGuruComponent;
  let fixture: ComponentFixture<EditGuruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGuruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGuruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
