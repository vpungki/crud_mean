import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service.service';
import { FormBuilder, FormGroup, Validator, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-guru',
  templateUrl: './edit-guru.component.html',
  styleUrls: ['./edit-guru.component.css']
})
export class EditGuruComponent implements OnInit {

  editForm: FormGroup;
  constructor(private router: Router, private serviceService: ServiceService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let guruId = window.localStorage.getItem("editGuruId");

    if (!guruId) {
      this.router.navigate(['guru']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id: [],
      nik: [],
      name: [],
      tgllhr: [],
      jabatan: [],
      gaji: [],
      __v: []
    });

    this.serviceService.getGuruById(guruId).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this.serviceService.putGuru(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Sukses");
        window.localStorage.removeItem("guruId");
        this.router.navigate(['guru']);
      } catch (err) {
        console.log(err.message);
      }
    })
  }

}
